import math

def epsilon(M, N, delta):
	return math.sqrt(1/(2*N) * math.log(2*M / delta))

delta = 0.03
M = 10000
N = 1

while epsilon(M, N, delta) > 0.05:
	N += 1

print(N)