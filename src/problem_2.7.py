import numpy as np
import matplotlib.pyplot as plt
import math

N = np.arange(10)

def problem25(N, dvc):
	return math.pow(N, dvc) + 1

def problem26(N, dvc):
	return math.pow(math.e*N/dvc, dvc)



bound25_dvc2 = [problem25(n, 2) for n in N]

bound26_dvc2 = [problem26(n, 2) for n in N]

bound25_dvc5 = [problem25(n, 5) for n in N]

bound26_dvc5 = [problem26(n, 5) for n in N]

plt.xlabel('N')
plt.title('dvc = 2')
label1, = plt.plot(N, bound25_dvc2, 'b--', label='problem 2.5')
label2, = plt.plot(N, bound26_dvc2, 'r', label='problem 2.6')
plt.legend(handles=[label1, label2])
plt.show()

plt.xlabel('N')
plt.title('dvc = 5')
label1, = plt.plot(N, bound25_dvc5, 'b--', label='problem 2.5')
label2, = plt.plot(N, bound26_dvc5, 'r', label='problem 2.6')
plt.legend(handles=[label1, label2])
plt.show()


