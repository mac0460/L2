import math

epsilon = 0.05
delta = 0.05
dvc = 10
N = 1

def sample_complexity(epsilon, delta, dvc, N):
	return (8/math.pow(epsilon, 2))*math.log((4*math.pow(2*N, dvc) + 4)/delta)

while (N < sample_complexity(epsilon, delta, dvc, N)):
	N += 1

print("tamanho da amostra deve ser: ", N)


# print("para uma amostra de tamanho 1000, o sample_complexity é: ", sample_complexity(epsilon, delta, dvc, 1000))
